from site_user import user
from flask import render_template, request, flash, session, abort
from forms import SignUp_form
from site_user.model import Users
from hashlib import md5
from app import db
from forms import search, ChangePassword


@user.route("/SignUp", methods=['POST', 'GET'])
def SignUp_user():
    SignUp = SignUp_form()
    return render_template('SignUp.html', login=SignUp)


@user.route('/create_user', methods=['POST'])
def create_user():
    creat = SignUp_form(request.form)
    info = {'name': creat.name.data,
            'email': creat.email.data,
            'user': creat.user.data,
            'password': creat.password.data,
            'repassword': creat.repassword.data}
    if info['password'] == info['repassword']:
        try:
            h = md5(info['password'].encode())
            info['password'] = h.hexdigest()
            new_user = Users(name=info['name'], email=info['email'],
                             user=info['user'], password=info['password'])
            db.session.add(new_user)
            db.session.commit()
            session['user'] = info['user']
            return ' user crated'
        except:
            flash(' !!!شخصی با این اطلاعات قبلا ثبت نام کرده است')
            return render_template('SignUp.html', login=creat)

    else:
        flash(' !!!پسورد ها یکی نیست')
        return render_template('SignUp.html', login=creat)


@user.route('/login')
def user_login():
    create = SignUp_form()
    return render_template('login.html', login=create)


@user.route('/login_user', methods=['POST'])
def veryfi():
    login = SignUp_form(request.form)
    info = {'user': login.user.data, 'password': login.password.data}
    h = md5(info['password'].encode())
    info['password'] = h.hexdigest()
    info_user = Users.query.filter_by(user=info['user'], password=info['password']).first()
    if info_user is None:
        flash(' !!!یوزر یا پسورد اشتباه است ')
        return render_template('login.html', login=login)
    session['user'] = info['user']
    search_form = search()
    return render_template('after_login.html', search_form = search_form)


@user.route('/logout')
def logout():
    session.clear()
    return render_template('Logout.html')


@user.route('/profile/<string:user>', methods=['GET'])
def profile(user):
    if session.get('user'):
        if session['user'] == user:
            search_form = search()
            info_user = Users.query.filter_by(user=user).first()
            return render_template('Profile.html', search_form=search_form, info=info_user)
        else:
            return("خطای دسترسی")
    else:
        return("هنوز لاگین نکردی ")

@user.route('/change_password/<string:user>', methods = ['GET'])
def change_password(user):
    if session.get('user') == user:
        change_password_form = ChangePassword()
        return render_template('change_password.html', change_pass = change_password_form)
    else:
        return abort(401)

@user.route('/changed_password/<string:user>', methods = ['POST'])
def changed_password(user):
    if session.get('user') == user :
        user_info = Users.query.filter_by(user=user).first()
        old_password =user_info.password
        passwords = ChangePassword(request.form)
        if passwords.new.data == passwords.confirm.data:
            h = md5(passwords.new.data.encode())
            new_pass = h.hexdigest()
            h = md5(passwords.old.data.encode())
            old_pass_form = h.hexdigest()
            if old_password == old_pass_form:
                user_info.password = new_pass
                db.session.commit()
                return 'پسورد شما با موفقیت تغییر یافت'
            else:
                return 'پسورد شما با پسورد قیلی مطابقت ندارد'
        else :
            return'پسورد یکی نیست'
    else:
        return abort(401)