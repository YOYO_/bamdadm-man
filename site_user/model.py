from app import db
from sqlalchemy import Column, Integer, String


class Users(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    email = Column(String(120), unique=True, nullable=False)
    user = Column(String(80), unique=True, nullable=False)
    password = Column(String(32), nullable=False)
