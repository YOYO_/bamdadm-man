FROM python:3.7-slim-stretch

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt
RUN pip3 install gunicorn

COPY . /app

CMD ["sh", "docker-entry.sh"]