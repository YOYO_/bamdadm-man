from app import db
from sqlalchemy import Column, Integer, String, Text


class Music(db.Model):
    id = Column(Integer, primary_key=True)
    name = Column(String(32), nullable=False)
    album = Column(String(32))
    link = Column(String(64), nullable=False)
    link_download = Column(String(64), nullable=False)
    info = Column(Text, nullable=False)
    artist = Column(String(32), nullable=False)
    photo = Column(String(64), nullable=False)


class Like(db.Model):
    id = Column(Integer, primary_key=True)
    id_user = Column(Integer)
    id_music = Column(Integer)