from flask import render_template, request
from site_musics import musics
from site_musics.model import Music
from sqlalchemy import or_
from forms import search


@musics.route('/')
def index():
    info_music = Music.query.all()
    search_form = search()
    return render_template('index.html', musics=info_music, search_form=search_form)


@musics.route('/Music/<string:name>')
def single_music(name):
    post = Music.query.filter(Music.name == name).first_or_404()
    search_form = search()
    return render_template('single_music.html', post=post, search_form=search_form)


@musics.route('/search')
def music_search():
    search_query = request.args.get('Search', '')
    name = Music.name.ilike(f'%{search_query}%')
    album = Music.album.ilike(f'%{search_query}%')
    artist = Music.artist.ilike(f'%{search_query}%')
    found_posts = Music.query.filter(or_(name,
                                         album,
                                         artist)).all()
    search_form = search()
    return render_template('search_music.html' ,search_form = search_form , found_posts = found_posts )