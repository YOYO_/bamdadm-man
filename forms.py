from flask_wtf import FlaskForm
from wtforms import TextField , PasswordField
from wtforms.validators import DataRequired 


class SignUp_form(FlaskForm):
    name = TextField(validators=[DataRequired()])
    email = TextField(validators=[DataRequired()])
    user = TextField(validators=[DataRequired()])
    password = PasswordField(validators=[DataRequired()])
    repassword = PasswordField(validators=[DataRequired()])

class search(FlaskForm):
    Search = TextField(validators=[DataRequired()])


class ChangePassword(FlaskForm):
    old = TextField(validators = [DataRequired()])
    new = TextField(validators = [DataRequired()])
    confirm = TextField(validators = [DataRequired()])