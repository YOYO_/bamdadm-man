var song = document.querySelector(".song")
var juice = document.querySelector(".green-juice")
var btn = document.getElementById("play-pause")

function togglePlayPause(){
    if(song.paused){
        btn.className = "pause";
        song.play();
    }
    else{
        btn.className = "play";
        song.pause();
    }
}
btn.onclick = function(){
    togglePlayPause();
}