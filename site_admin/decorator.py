from flask import session , abort
from functools import wraps

def admin_only_view(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        if session.get('user') is None:
            return abort(401)
        if not session['user']=='ali':
            return abort(403)
        return func(*args, **kwargs)
    return decorator