from flask_wtf import FlaskForm
from wtforms import TextField , FileField
from wtforms.validators import DataRequired


class music_form(FlaskForm):
    name = TextField(validators=[DataRequired()])
    album = TextField()
    link = TextField(validators=[DataRequired()])
    link_download = TextField(validators=[DataRequired()])
    info = TextField(validators=[DataRequired()])
    artist = TextField(validators=[DataRequired()])
    photo = FileField(validators=[DataRequired()])
    file = FileField(validators=[DataRequired()])
