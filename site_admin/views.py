from flask import render_template, request, session, redirect, url_for
from site_admin import admin
from site_admin.form import music_form
from site_musics.model import Music
from app import db
from site_admin.decorator import admin_only_view
from site_user.model import Users
from werkzeug.utils import secure_filename


@admin.route('/')
@admin_only_view
def admin_index():
    return render_template('admin_index.html')


@admin.route('/info_new_music')
@admin_only_view
def info_new_music():
    Music_form = music_form()

    return render_template('new_music.html', form=Music_form)


@admin.route('/create_music', methods=['POST'])
@admin_only_view
def create():
    Music_form = music_form()
    info_music = {'name': Music_form.name.data, 'album': Music_form.album.data, 'link': Music_form.link.data,
                  'link_download': '/static/Music/' + secure_filename(Music_form.file.data.filename),
                  'info': Music_form.info.data,
                  'artist': Music_form.artist.data, 'photo': '/static/img/' + secure_filename(Music_form.photo.data.filename)}
    new_music = Music(
        name=info_music['name'], album=info_music['album'], link=info_music['link'],
        link_download=info_music['link_download'], info=info_music['info'], artist=info_music['artist'],
        photo=info_music['photo'])
    Music_form.file.data.save('static/Music/' + secure_filename(Music_form.file.data.filename))
    Music_form.photo.data.save('static/img/' + secure_filename(Music_form.photo.data.filename))
    db.session.add(new_music)
    db.session.commit()
    return ('created music : \n name : %s \n album : %s \n link : %s \n link download : %s \n info : %s \n artist : '
            '%s \n photo : %s \n' % (info_music['name'], info_music['album'], info_music['link'], info_music[
        'link_download'], info_music['info'], info_music['artist'], info_music['photo']))


@admin.route('/User_list')
@admin_only_view
def user_list():
    users_list = Users.query.all()
    return render_template('user_list.html', users_list=users_list)


@admin.route('/user_delete/<int:user_id>')
@admin_only_view
def delete_user(user_id):
    user = Users.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('admin.user_list'))


@admin.route('/music_list')
@admin_only_view
def music_list():
    musics_list = Music.query.all()
    return render_template('music_list.html', music_list=musics_list)


@admin.route('/music_delete/<int:music_id>')
@admin_only_view
def delete_music(music_id):
    music = Music.query.get_or_404(music_id)
    db.session.delete(music)
    db.session.commit()
    return redirect(url_for('admin.music_list'))
